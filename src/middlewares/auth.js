export default function auth({
    next,
    router
}) {
    if (!localStorage.getItem('user_token')) {
        return router.push({
            name: 'Login'
        })
    }

    return next()
}