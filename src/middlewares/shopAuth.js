import axios from "axios"

export default async function auth({
    next,
    router
}) {
    const getShopHeaders = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer " + localStorage.getItem('user_token'),
    }
    if (!localStorage.getItem('shop_token')) {
        const shops = await axios({
            url: `https://api-olshop.ascomycota.my.id/api/shops`,
            headers: getShopHeaders,
        });

        if (typeof shops.data.data !== 'undefined' && shops.data.data.length > 0) {
            const shop_token = await axios({
                method: "POST",
                url: `https://api-olshop.ascomycota.my.id/api/login-shop`,
                headers: getShopHeaders,
                data: {
                    shop_id: shops.data.data[0].id
                }
            })
            if (typeof shop_token.data !== 'undefined' ) localStorage.setItem('shop_token', shop_token.data.data)
        } else {
            return router.push({
                name: 'CreateShop'
            })
        }
    }

    return next()
}