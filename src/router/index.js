import Vue from 'vue'
import VueRouter from 'vue-router'

// Main Page
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import UserProfile from '../views/UserProfile.vue'
import CreateShop from '../views/CreateShop.vue'

// Seller Page
import SellerHome from '../views/Seller/Home.vue'
import Product from '../views/Seller/Product.vue'
import AddProduct from '../views/Seller/AddProduct.vue'
import EditProduct from '../views/Seller/EditProduct.vue'
import ProductCategory from '../views/Seller/ProductCategory.vue'
import Discount from '../views/Seller/Discount.vue'

// Buyer Page
import BuyerHome from '../views/Buyer/Home.vue';
import BuyerProduct from '../views/Buyer/DetailProduct.vue';
import BuyerCart from '../views/Buyer/Cart.vue';
import BuyerCheckout from '../views/Buyer/Checkout.vue';

// Middlewares
import Auth from '../middlewares/auth'
import ShopAuth from '../middlewares/shopAuth'
Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/register',
        name: 'Register',
        component: Register
    },
    {
        path: '/profile',
        name: 'UserProfile',
        component: UserProfile
    },
    {
        path: '/seller',
        name: 'SellerHome',
        component: SellerHome,
        meta: {
            middleware: [
                Auth,
                ShopAuth
            ]
        }
    },
    {
        path: '/create-shop',
        name: 'CreateShop',
        component: CreateShop,
        meta: {
            middleware: Auth
        }
    },
    {
        path: '/seller-product',
        name: 'Product',
        component: Product,
        meta: {
            middleware: [
                Auth,
                ShopAuth
            ]
        }
    },
    {
        path: '/seller-product/add',
        name: 'AddProduct',
        component: AddProduct,
        meta: {
            middleware: [
                Auth,
                ShopAuth
            ]
        }
    },
    {
        path: '/seller-product/:id',
        name: 'EditProduct',
        component: EditProduct,
        meta: {
            middleware: [
                Auth,
                ShopAuth
            ]
        }
    },
    {
        path: '/seller-product-category',
        name: 'ProductCategory',
        component: ProductCategory,
        meta: {
            middleware: [
                Auth,
                ShopAuth
            ]
        }
    },
    {
        path: '/seller-discount',
        name: 'Discount',
        component: Discount,
        meta: {
            middleware: [
                Auth,
                ShopAuth
            ]
        }
    },
    {
        path: '/shop/:shopUrl',
        name: "BuyerHome",
        component: BuyerHome
    },
    {
        path: '/shop/:shopUrl/:itemId',
        name: "BuyerProduct",
        component: BuyerProduct
    },
    {
        path: '/shop/:shopUrl/:cart',
        name: "BuyerCart",
        component: BuyerCart
    },
    {
        path: '/shop/:shopUrl/:checkout',
        name: "BuyerCheckout",
        component: BuyerCheckout
    }
]

const router = new VueRouter({
    routes,
    mode: "history",
    scrollBehavior() {
        return {
            x: 0,
            y: 0
        };
    },
})

export default router