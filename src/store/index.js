import Vue from 'vue'
import Vuex from 'vuex'
import productCategory from './modules/productCategory';
import product from './modules/product';
import discount from './modules/discount';

Vue.use(Vuex)

export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    modules: {
        productCategory,
        product,
        discount
    }
})