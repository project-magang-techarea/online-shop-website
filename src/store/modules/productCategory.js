import axios from 'axios';

const state = {
    productCategory: [],
};
const getters = {
    allProductCategory: (state) => state.productCategory
};
const actions = {
    async fetchProductCategory({ commit }) {
        // Dynamic token
        const token = localStorage.getItem('shop_token');
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        // eslint-disable-next-line
        const productCategory = await axios.get(`${$api}/product-categories`, config);
        commit('setProductCategory', productCategory.data);
    }
};
const mutations = {
    setProductCategory: (state, productCategory) => (state.productCategory = productCategory),
};

export default {
    state,
    getters,
    actions,
    mutations
}