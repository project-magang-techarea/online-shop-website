import axios from 'axios';

const state = {
    discounts: [],
};
const getters = {
    allDiscount: (state) => state.discounts
};
const actions = {
    async fetchDiscount({ commit }) {
        // Dynamic token
        const token = localStorage.getItem('shop_token');
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        // eslint-disable-next-line
        const discount = await axios.get(`${$api}/discounts`, config);
        commit('setDiscount', discount.data);
    }
};
const mutations = {
    setDiscount: (state, discounts) => (state.discounts = discounts),
};

export default {
    state,
    getters,
    actions,
    mutations
}