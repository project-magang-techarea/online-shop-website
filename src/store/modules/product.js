import axios from 'axios';

const state = {
    products: [],
};
const getters = {
    allProduct: (state) => state.products
};
const actions = {
    async fetchProducts({ commit }) {
        // Dynamic token
        const token = localStorage.getItem('shop_token');
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        // eslint-disable-next-line
        const products = await axios.get(`${$api}/products`, config);
        commit('setProducts', products.data);
    }
};
const mutations = {
    setProducts: (state, products) => (state.products = products),
};

export default {
    state,
    getters,
    actions,
    mutations
}