import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import VueGlobalVar from 'vue-global-var';
import * as VueGoogleMaps from "vue2-google-maps";

Vue.config.productionTip = false

Vue.use(VueGlobalVar, {
    globals: {
        $api: 'https://api-olshop.ascomycota.my.id/api',
        $storage: 'https://api-olshop.ascomycota.my.id/storage',
    }
});

Vue.use(VueGoogleMaps, {
    load: {
        key: "AIzaSyBEK7Z2VxfF6275j0GH-mXPJ3fd8pPYDXs",
    },
    installComponents: true,
});

Vue.mixin({
    methods: {
        convertToRupiah(angka, prefix) {
            angka = angka.toString();
            var number_string = angka.replace(/[^,\d]/g, "").toString(),
                split = number_string.split(","),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            if (ribuan) {
                var separator = sisa ? "." : "";
                rupiah += separator + ribuan.join(".");
            }

            rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
            return prefix == undefined ? rupiah : rupiah ? "Rp" + rupiah : "";
        },
    }
})

new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app')