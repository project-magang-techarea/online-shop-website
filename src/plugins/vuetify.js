import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

const vuetify = new Vuetify({
    theme: {
        themes: {
            light: {
                primary: '#EE5577',
                accent: '#FFD9DA',
                secondary: '#2E3159',
                seller: '#EFF3F8',
            },
        },
    },
})

export default vuetify